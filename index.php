<?php
session_start();
$items = $_SESSION['items'];
$currencyUah = $_SESSION['currencyUah'];
$currencyUsd = $_SESSION['currencyUsd'];
$currencyEur = $_SESSION['currencyEur'];




?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=p, initial-scale=1.0">
    <title>Валюта</title>
    <meta name="description" content="Choose currency">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <span class="navbar-brand mb-0 h1">Tech-sycho - твой любимый магазин с гаджетами.</span>
            </div>
        </nav>
        <div>
            <ul class="nav navbar-dark bg-dark">
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Главная страница</a>
            </li>
            </ul>
        </div>
    </div>
    <div class="container">
    <h3>Выбор валюты для просмотра стоимости товаров</h3>
    <div class="row justify-content-center">
    <form action="choose_currency.php" method="post">
        <div>
            <label>
                Валюта:
                <select name="currency" class="form-select form-select-md mb-3" aria-label=".form-select-md example">
                    <option value='none' selected>Выберите валюту</option>
                    <option value='uah'>Гривна</option>
                    <option value="usd">Доллар</option>
                    <option value="eur">Евро</option>
                </select>
            </label>
        </div>
        <div class="mb-3">
            <button class="btn btn-primary white-text">Выбрать</button>
        </div>
    </form>
    <div class="container">
    <div class="row">
        <table class="table table-success table-striped">
            <thead>
            <tr>
                <th scope="col">Продукт</th>
                <th scope="col">Цена</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($items as $item):?>
            <tr>
                <td><?=$item['title'];?></td>
                <td>
                    <?php if($currencyUah):?>
                    <?=$item['price_val'] = round($item['price_val']/$currencyUah['course'], 2);;?>
                    <?php elseif($currencyUsd):?>
                    <?=$item['price_val'] = round($item['price_val']/$currencyUsd['course'], 2);?>
                    <?php elseif($currencyEur):?>
                    <?=$item['price_val'] = round($item['price_val']/$currencyEur['course'], 2);?>
                    <?php endif;?>
                </td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>
</html>